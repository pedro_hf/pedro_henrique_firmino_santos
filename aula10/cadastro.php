<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cadastro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <?php
        if(isset($_POST['enviar'])):
            if(in_array("", $_POST)):
                $msm = "Favor preencha todos os campos";
            else:
                include('cadastrar.php');
            endif;
        endif;

    ?>
    <?=isset($msm)? "<p>".$msm."</p>" : null  ?>
    <form method="POST">
        <input type="text" name="nome" placeholder="Nome:"><br><br>
        <input type="password" name="senha" placeholder="Senha:"><br><br>
        <input type="email" name="email" placeholder="E-mail:"><br><br>
        <input type="submit" name="enviar" value="Enviar">
    </form>
</body>
</html>