<?php
    if(!$db = mysqli_connect('localhost', 'root', '', 'aulasphp')):
        echo "Problema ao conectar ao SGDB";
        exit;
    endif;

    

    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    $prep = mysqli_prepare($db, "INSERT INTO login (user_nome, user_email, user_senha) VALUES (?,?,?)");
    mysqli_stmt_bind_param($prep, 'sss', $post['nome'], $post['email'], $post['senha']);   
    if(mysqli_stmt_execute($prep)): 
        header('Location: login.php?cadastrado');
    endif;
