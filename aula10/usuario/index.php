<?php

session_start();

if (!session_id()):
    header('Location: ../login.php');
    exit();
endif;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$_SESSION['user']['user_nome']?></title>
</head>
<body>
    <h1>Ola, <?=$_SESSION['user']['user_nome']?></h1>
    <hr>
    <p><b>Nome:</b> <?=$_SESSION['user']['user_nome']?></p>
    <p><b>E-mail:</b> <?=$_SESSION['user']['user_email']?></p>
    <a href="editar.php">Editar</a>
</body>
</html>