<?php 
class Conn{

	private $Host = "localhost";
    private $User = "root";
    private $Pass = "";
    private $Dbsa = "aula14";

    public function Conectar() {
		try{
		$pdo = new PDO("mysql:dbname=".$this->Dbsa."; host=".$this->Host.";port=3308",$this->User, $this->Pass);

		} catch (PDOException $e){
		    echo "Erro com banco de dados: ".$e->getMessage();
		}
		catch(Exception $e){
		    echo "Erro generico: ".$e->getMessage();
		}

        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $pdo;

    }


} 