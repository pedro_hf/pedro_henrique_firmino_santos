<?php
    if(!$db = mysqli_connect('localhost', 'root', '', 'aulasphp')):
        echo "Problema ao conectar ao SGDB";
        exit;
    endif;

    
if(isset($_POST['enviar'])):
    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);

    // $prep = mysqli_prepare($db, "INSERT INTO login (nome, email, senha) VALUES (?,?,?)");
    // mysqli_stmt_bind_param($prep, 'sss', $post['nome'], $post['email'], $post['senha']);   
    // if(mysqli_stmt_execute($prep)): 
    //     echo "<br><br>Dados de {$post['nome']} gravados no SGBD!"; 
    // endif;

    if(in_array("", $post)):
        echo "<h2>Erro você deve preencher todos os campos antes de prosseguir</h2>";
    else:
        $consulta = mysqli_query($db, 'SELECT * FROM login');
        $result = $consulta->fetch_assoc(); 
        
        if($result['nome'] != $post['nome']):
            $nome = false;
        else:
            $nome = true;
        endif;

        if($result['senha'] != $post['senha']):
            $senha = false;
        else:
            $senha = true;
        endif;

        if($result['email'] != $post['email']):
            $email = false;
        else:
            $email = true;
        endif;

        if($nome && $senha && $email): 
            session_start();
            $_SESSION['nome_usuer'] = $result['nome'];
            $_SESSION['user_id'] = $result['id_login']; 
            include('menu.php');
            exit;
            
        else:
            echo "Erro";

        endif;
    endif;
endif;

echo "<form method=\"POST\">
        Nome: <input type=\"text\" name=\"nome\"><br><br>
        Senha: <input type=\"password\" name=\"senha\"><br><br>
        E-mail: <input type=\"email\" name=\"email\"><br><br>
        <input type=\"submit\" name=\"enviar\" value=\"Enviar\">
    </form>";

