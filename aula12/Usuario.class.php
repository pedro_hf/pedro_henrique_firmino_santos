<?php 

class Usuario {

    private  $id;
    private  $nome;
    private  $senha;
    private  $email;
    private  $db;

    public function __construct(){
        $this->db = new mysqli('localhost', 'root', '', 'aulasphp', 3307);
        // var_dump($this->db);
    }

    public function setId(int $id){
        $this->id = $id;    
    }
    public function setNome(string $nome){
        $this->nome = $nome;    
    }
    public function setEmail(string $email){
        $this->email = $email;    
    }
    public function setSenha(string $senha){
        $this->senha = $senha;    
    }
    
    public function saveUsuario(){
        $prep = $this->db->prepare('INSERT INTO usuario (nome, senha, email) VALUES (?, ?, ?)');
        $prep->bind_param('sss', $this->nome, password_hash($this->senha, PASSWORD_DEFAULT  ), $this->email); 
        if($prep->execute()):
            echo "<br><br>Dados de $this->nome gravados no SGBD!";
        endif;
    }
    
    public function getId(int $id){
            $consulta = $this->db->prepare('SELECT * FROM usuario WHERE id = ' . $id);
            $consulta->execute();
            $resut = $consulta->get_result();
            $resut->fetch_assoc();
            var_dump($resut);    
    }

    public function delet(int $id){
        $delet = $this->db->prepare('DELETE FROM usuario WHERE id = ' . $id);
        $delet->execute();

}

    // public function getNome(string $nome):string{
    //    return $this->nome;    
    // }
    // public function getEmail(string $email):string{
    //     return $this->email;    
    //  }
    // public function getSenha(string $senha):string{
    //    return $this->senha;    
    // }

    public function __destruct(){
        echo "<br>Fechando a conexão com o SGDB";
    }
    
}